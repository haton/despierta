#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Help:
    def getHelp():
        return('''-v --version\t\t\t\tShow version\n-h --help\t\t\t\tShow this help\n-t --time\t\t\t\tSet alarm time\n-s --song\t\t\t\tSet the song\n-m --modules\t\t\t\tSet the modules''')
