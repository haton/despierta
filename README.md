# DesPIerta 0.1

DesPIerta is a freesoftware to wake up in a good mood.

## License

DesPIerta is free software. Check LICENSE file for more information.

## Install

The only thing that you need is mpv player.

	`pacman -S mpv`
