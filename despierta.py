#!/usr/bin/env python3
# -*- coding: utf-8 -*-

######################################################
# DesPIerta is a software to wake up in a good mood  #
# haton - <haton@elbinario.net>                      #
# fanta - <fanta@elbinario.net>                      #
######################################################

import sys
if sys.version_info<(3,1,4):
    sys.stderr.write("Need python 3.1.4 or later\n")
    exit(1)
import datetime
import threading
from src.help import Help
from time import sleep
from os import system, getenv
import configparser
import argparse

# DesPIerta variables
name = "DesPIerta"
version = "0.1"

def parsearArgumentos():
    parser = argparse.ArgumentParser(description="desPIerta is a software to wake up in a good mood")
    parser.add_argument('-v', '--version',  action='version', version="desPIerta " + version, help='show the version')
    parser.add_argument('-c', '--config',  action='store', dest='config', help='change route to config file')
    parser.add_argument('-s', '--song', action='store', dest='song', help='path to the song')
    parser.add_argument('-t', '--time', action='store', dest='time', help='set time')

    return parser.parse_args()

def incrementarVolumen():
    for i in range(1,10):
        sleep(10)
        system("amixer sset Master 5%+")
    return

def reproducirCancion(reproductor, song):
    system(reproductor + " " + song)

def configSectionMap(section, config):
    dic = {}
    for e in config.items(section):
        dic[e[0]] = e[1]
    return dic

def main():
    args = parsearArgumentos()
    #Comprobamos archivo de configuración
    config_path = getenv("HOME")+'/.despierta/config'

    if args.config is not None:
        config_path = args.config

    config = configparser.RawConfigParser()
    #Sacamos la configuración del archivo de configuración
    try:
        config.read(config_path)
        dic = configSectionMap('Default', config)
        alarm_hour = int(dic['hour'])
        alarm_min = int(dic['minute'])
        song = dic['song']
    except TypeError:
        print("Alerta: no se ha definido ningún archivo de configuración")

    #Sacamos la configuración de los comandos y la sobreescribimos
    if args.song is not None:
        song = args.song

    if args.time is not None:
        alarm_hour = int(args.time.split(":")[0])
        alarm_min = int(args.time.split(":")[1])

    #Si existe la variable alarm_hour, hay una alarma programada
    if 'alarm_hour' in locals() and 'alarm_min' in locals() and 'song' in locals():
        #Smart way
        if alarm_min > 10:
            print("La alarma está programada para las " + str(alarm_hour) + ":" + str(alarm_min))
        else:
            print("La alarma está programada para las " + str(alarm_hour) + ":0" + str(alarm_min))

        while True:
            sleep(2)
            now = datetime.datetime.now()
            if now.hour == alarm_hour and now.minute == alarm_min:
                #Bajamos el volumen al 30%
                system("amixer sset Master 30%")

                #Separamos en un thread el incremento progresivo del volumen
                thread_amixer = threading.Thread(target=incrementarVolumen)
                thread_amixer.start()
                reproducirCancion('mpv', song)
                break
    else:
        if not 'alarm_hour' in locals() or not 'alarm_min' in locals():
            print("No se ha definido ninguna hora para la alarma.")
        if not 'song' in locals():
            print("No se ha definido ninguna canción.")

if __name__ == '__main__':
    main()
